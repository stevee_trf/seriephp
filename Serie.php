<?php

class Serie {
    protected $nombre;
    protected $genero;
    protected $temporadas = [];

    function __construct(string $nombre, string $genero) {
        $this->nombre = $nombre;
        $this->genero = $genero;
    }

    /*
    public function getTemporada() {
        $valor = implode(",", $this->temporadas);
        return $valor;
    }*/

    public function getValoracionTotal() {
        $valoraciones_temp = 0;
        $n_capitulos = 0;
        //Guardamos las valoraciones de los capítulos de cada temporada, y también, guaradamos el número de capítulos de cada temporada
        for($z = 0; $z < count($this->temporadas); $z = $z + 1) {
            $valoraciones_temp = $valoraciones_temp + $this->temporadas[$z]->getValoraciones();
            $n_capitulos = $n_capitulos + $this->temporadas[$z]->getNCapitulos();
            //echo count($this->temporadas);
        }

        return "La valoración de los " . $n_capitulos . " capitulo(s) entre las " . count($this->temporadas) . " temporada(s) es: " . ($valoraciones_temp / $n_capitulos);
    }

    public function addTemporada(Temporada $temporada) {
        array_push($this->temporadas, $temporada);
    }
}

class Temporada extends Serie {
    protected $n_temporada;
    protected $n_capitulos;
    protected $ano_temporada;
    protected $capitulos = [];

    function __construct(int $n_temporada, int $ano_temporada) {
        $this->n_temporada = $n_temporada;
        $this->ano_temporada = $ano_temporada;
    }

    public function getNCapitulos() {
        return $this->n_capitulos;
    }

    public function getNTemporadas() {
        return $this->n_temporada;
    }

    public function getCapitulo() {
        return $this->capitulos;
    }

    public function getValoraciones() {
        $resultado = 0;
        //Guaradamos las valoraciones de todos los capítulos de cada temporada
        for($z = 0; $z < count($this->capitulos); $z = $z + 1) {
            $resultado = $resultado + $this->capitulos[$z]->getValoracion();
        }

        return $resultado;
    }

    public function addCapitulo(Capitulo $capitulo) {
        $this->n_capitulos += 1;
        array_push($this->capitulos, $capitulo);
    }
}

class ElementoMultimedia extends Serie {
    protected $duracion;
}

class Capitulo extends ElementoMultimedia {
    protected $titulo;
    protected $fecha_estreno;
    protected $valoracion;
    protected $n_capitulo;

    function __construct(string $titulo, $fecha_estreno, int $valoracion, int $n_capitulo) {
        $this->titulo = $titulo;
        $this->fecha_estreno = $fecha_estreno;
        $this->valoracion = $valoracion;
        $this->n_capitulo = $n_capitulo;
    }

    public function getValoracion() {
        return $this->valoracion;
    }
}

$cap1_1 = new Capitulo('Capítulo1', '2021-02-16', 10, 1);
$cap2_1 = new Capitulo('Capítulo2', '2021-02-16', 10, 1);
$cap1_2 = new Capitulo('Capítulo1', '2021-02-16', 10, 2);
$cap1_3 = new Capitulo('Capítulo1', '2021-02-16', 10, 3);

$temp1 = new Temporada(1, 2021);
$temp2 = new Temporada(2, 2021);
$temp3 = new Temporada(3, 2021);
$temp1->addCapitulo($cap1_1);
$temp1->addCapitulo($cap2_1);
$temp2->addCapitulo($cap1_2);
$temp3->addCapitulo($cap1_3);

$serie1 = new Serie('Serie1', 'Ciencia ficción');
$serie1->addTemporada($temp1);
$serie1->addTemporada($temp2);
$serie1->addTemporada($temp3);

echo "<p>La temporada " . $temp1->getNTemporadas() . " tiene: " . $temp1->getNCapitulos() . " capitulo(s)</p>";
echo "<p>La temporada " . $temp2->getNTemporadas() . " tiene: " . $temp2->getNCapitulos() . " capitulo(s)</p>";
echo "<p>La temporada " . $temp3->getNTemporadas() . " tiene: " . $temp3->getNCapitulos() . " capitulo(s)</p>";
//echo "<p>" . $temp1->getValoraciones() . "</p>";
echo "<p>" . $serie1->getValoracionTotal() . "</p>";
?>